<!DOCTYPE html>
<%@tag description="Template Site tag" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html lang="en">
<head>

    <%--<meta charset="utf-8">--%>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>A theme with personality</title>

    <!-- Bootstrap Core CSS -->
    <%--<link href="css/bootstrap.min.css" rel="stylesheet">--%>
    <link rel='stylesheet' href='/webjars/bootstrap/4.0.0-alpha.3/css/bootstrap.min.css'>

    <!-- Custom CSS -->
    <%--<link href="css/modern-business.css" rel="stylesheet">--%>
    <%--<link href="../../resources/css/modern-business.css" rel="stylesheet"/>--%>
    <link href="../../resources/css/style.css" rel="stylesheet"/>

    <!-- Custom Fonts -->
    <%--<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">--%>
    <link href="../../resources/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900' rel='stylesheet'
          type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<nav class="navbar" id="nav">
    <div class="container">
        <a class="navbar-brand" href="index.html"><img alt="" src="../../resources/img/logo-nav.png"></a>
        <button aria-expanded="false" class="navbar-toggler hidden-md-up pull-right collapsed"
                data-target="#navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span>&#9776;
        </button>
        <div aria-expanded="false" class="navbar-toggleable-sm collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">

                <li class="nav-item">
                    <a href="<%=request.getContextPath()%>?languageVar=en">EN</a>
                    <a href="<%=request.getContextPath()%>?languageVar=ru">RU</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="/file.html"><spring:message code="navMenu.tutorial"/></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/jdbc.html">Jdbc Examples</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/email.html">Отправка Email =)</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/orm.html">ORM Hibernate</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/jstl.html">JSTL</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/scope.html">Session scope и Request scope</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/cookie.html">Cookie</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/rest.html">Rest</a>
                </li>
            </ul>
        </div>

        <nav class="nav social-nav pull-right hidden-sm-down">
            <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
                class="fa fa-linkedin"></i></a> <a href="#"><i class="fa fa-envelope"></i></a>
        </nav>
    </div>
</nav>
<!-- /Navbar -->
<!-- @component: content -->
<!-- About -->


<section class="cta" style="background: #ccc;">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-sm-12 text-lg-left text-xs-center">
                <security:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_SUPER_USER', 'ROLE_USER')" var="isUSer"/>

                <ul class="nav navbar-nav">
                    <c:if test="${not isUSer}">
                        <li style="padding-top: 15px; padding-bottom: 15px; color: red">
                            <c:if test="${empty param.error}">
                                Вы не вошли в приложение
                            </c:if>
                        </li>
                        <li><a style="color: Green;" href="<c:url value="/login.html"/>">Login</a></li>
                    </c:if>

                    <c:if test="${isUSer}">
                        <li style="padding-top: 15px; padding-bottom: 15px; color: green">
                            Вы вошли как:
                            <security:authentication property="principal.username"/> с ролью:
                            <b><security:authentication property="principal.authorities"/></b>

                        </li>
                        <li><a style="color: red;" href="<c:url value="/j_spring_security_logout"/>">Logout</a></li>
                    </c:if>
                </ul>
            </div>

            <div class="col-lg-3 col-sm-12 text-lg-right text-xs-center">
                <a class="btn btn-ghost" href="/security.html">Управление</a>
            </div>
        </div>
    </div>
</section>

<jsp:doBody/>

<footer class="site-footer" id="contact">
    <div class="container">
        <div class="row">
            <div class="footer-col col-sm-12 col-lg-6 text-xs-center text-lg-left">
                <h2>
                    Contact Us
                </h2>
                <a class="btn btn-full" href="#">Fast Contact</a>
                <nav class="nav social-nav footer-social-nav">
                    <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-facebook"></i></a> <a
                        href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i class="fa fa-envelope"></i></a> <a
                        href="#"><i class="fa fa-skype"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a>
                </nav>
            </div>

            <div class="footer-col col-sm-12 col-lg-6 text-xs-center text-lg-left">
                <p class="footer-text">
                    Lorem ipsum dolor sit amet, pro iusto offendit democritum ad. Alterum sapientem scribentur id vix,
                    ei vis saepe mucius complectitur. At eam omnis mucius persecuti, quidam salutatus nec no. Te vis
                    augue habemus. No sed meis clita civibus.
                </p>
            </div>
        </div>
    </div>

    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-xs-12">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#">Home</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="#">About Us</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="#">Features</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="#">Portfolio</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="#">Team</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-xs-12 text-lg-right text-xs-center">
                    <p class="copyright-text">
                        © BELL by BootBites
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a> <!-- JavaScript

    ================================================== -->
<!-- jQuery (version: 2.1.4) via googleapis or path to local copy ie. plugins/jquery/jquery.min.js -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!-- Tether plugin, needed for tooltip plugin -->

<script src="https://cdn.rawgit.com/HubSpot/tether/master/dist/js/tether.min.js"></script>
<!-- Required plugin: stellar.js http://markdalgleish.com/projects/stellar.js/ -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/stellar.js/0.6.2/jquery.stellar.min.js"></script>
<!-- Bootstrap (version: v4.0.0-alpha.2) JS via MaxCDN or path to local copy ie. plugins/bootstrap/dist/js/bootstrap.min.js-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<!-- Custom Javascript -->

<script src="../../resources/js/script.js"></script>

<script src="../../resources/plugins/jquery.lockfixed.min.js"></script>

<script src="../../resources/js/parallax.js"></script>

<!-- jQuery -->
<%--<script src="js/jquery.js"></script>--%>
<script src="/webjars/jquery/1.11.1/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<%--<script src="js/bootstrap.min.js"></script>--%>
<script src="/webjars/bootstrap/4.0.0-alpha.3/js/bootstrap.min.js"></script>

<!-- Script to Activate the Carousel -->
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>

</body>

</html>
