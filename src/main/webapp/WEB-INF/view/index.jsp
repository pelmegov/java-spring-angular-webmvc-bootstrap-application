<!DOCTYPE html>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="page" tagdir="/WEB-INF/tags" %>

<page:template>
    <jsp:body>
        <section class="about" id="about">
            <div class="container text-xs-center">
                <h2>
                    About Bell Theme
                </h2>

                <p>
                    Voluptua scripserit per ad, laudem viderer sit ex. Ex alia corrumpit voluptatibus usu, sed unum
                    convenire
                    id. Ut cum nisl moderatius, per nihil dicant commodo an. Eum tacimates erroribus ad. Atqui feugiat
                    euripidis
                    ea pri, sed veniam tacimates ex. Menandri temporibus an duo.
                </p>

                <div class="row stats-row">
                    <div class="stats-col text-xs-center col-md-3 col-sm-6">
                        <div class="circle">
                            <span class="stats-no" data-toggle="counter-up">232</span> Satisfied Customers
                        </div>
                    </div>

                    <div class="stats-col text-xs-center col-md-3 col-sm-6">
                        <div class="circle">
                            <span class="stats-no" data-toggle="counter-up">79</span> Released Projects
                        </div>
                    </div>

                    <div class="stats-col text-xs-center col-md-3 col-sm-6">
                        <div class="circle">
                            <span class="stats-no" data-toggle="counter-up">1,463</span> Hours Of Support
                        </div>
                    </div>

                    <div class="stats-col text-xs-center col-md-3 col-sm-6">
                        <div class="circle">
                            <span class="stats-no" data-toggle="counter-up">15</span> Hard Workers
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /About -->
        <!-- Parallax -->

        <section class="cta">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-sm-12 text-lg-left text-xs-center">
                        <h2>
                            Вызов Interceptors (перехватчики)
                        </h2>

                        <p>
                            Lorem ipsum dolor sit amet, nec ad nisl mandamus imperdiet, ut corpora cotidieque cum. Et
                            brute
                            iracundia his, est eu idque dictas gubergren
                        </p>
                    </div>

                    <div class="col-lg-3 col-sm-12 text-lg-right text-xs-center">
                        <a class="btn btn-ghost" href="/interceptorCall/subLevel">Interceptors (см в консоль)</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Call to Action -->
        <!-- Portfolio -->

        <section class="portfolio" id="portfolio">
            <div class="container text-xs-center">
                <h2>
                    Portfolio
                </h2>

                <p>
                    Voluptua scripserit per ad, laudem viderer sit ex. Ex alia corrumpit voluptatibus usu, sed unum
                    convenire
                    id. Ut cum nisl moderatius, Per nihil dicant commodo an.
                </p>
            </div>

            <div class="portfolio-grid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-1.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-2.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-3.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-4.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-5.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-6.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-7.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="card card-block">
                            <a href="#"><img alt="" src="../../resources/img/porf-8.jpg">
                                <div class="portfolio-over">
                                    <div>
                                        <h3 class="card-title">
                                            The Dude Rockin'
                                        </h3>

                                        <p class="card-text">
                                            Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Portfolio -->
        <!-- Team -->

        <section class="team" id="team">
            <div class="container">
                <h2 class="text-xs-center">
                    Meet our team
                </h2>

                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="card card-block">
                            <a href="#"><img alt="" class="team-img" src="../../resources/img/team-1.jpg">
                                <div class="card-title-wrap">
                                    <span class="card-title">Sergio Fez</span> <span
                                        class="card-text">Art Director</span>
                                </div>

                                <div class="team-over">
                                    <h4 class="hidden-md-down">
                                        Connect with me
                                    </h4>

                                    <nav class="nav social-nav">
                                        <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
                                            class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i
                                            class="fa fa-envelope"></i></a>
                                    </nav>

                                    <p>
                                        Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <div class="card card-block">
                            <a href="#"><img alt="" class="team-img" src="../../resources/img/team-2.jpg">
                                <div class="card-title-wrap">
                                    <span class="card-title">Sergio Fez</span> <span
                                        class="card-text">Art Director</span>
                                </div>

                                <div class="team-over">
                                    <h4 class="hidden-md-down">
                                        Connect with me
                                    </h4>

                                    <nav class="nav social-nav">
                                        <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
                                            class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i
                                            class="fa fa-envelope"></i></a>
                                    </nav>

                                    <p>
                                        Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <div class="card card-block">
                            <a href="#"><img alt="" class="team-img" src="../../resources/img/team-3.jpg">
                                <div class="card-title-wrap">
                                    <span class="card-title">Sergio Fez</span> <span
                                        class="card-text">Art Director</span>
                                </div>

                                <div class="team-over">
                                    <h4 class="hidden-md-down">
                                        Connect with me
                                    </h4>

                                    <nav class="nav social-nav">
                                        <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
                                            class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i
                                            class="fa fa-envelope"></i></a>
                                    </nav>

                                    <p>
                                        Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-6">
                        <div class="card card-block">
                            <a href="#"><img alt="" class="team-img" src="../../resources/img/team-4.jpg">
                                <div class="card-title-wrap">
                                    <span class="card-title">Sergio Fez</span> <span
                                        class="card-text">Art Director</span>
                                </div>

                                <div class="team-over">
                                    <h4 class="hidden-md-down">
                                        Connect with me
                                    </h4>

                                    <nav class="nav social-nav">
                                        <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
                                            class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i
                                            class="fa fa-envelope"></i></a>
                                    </nav>

                                    <p>
                                        Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Team -->
        <!-- @component: footer -->

    </jsp:body>
</page:template>