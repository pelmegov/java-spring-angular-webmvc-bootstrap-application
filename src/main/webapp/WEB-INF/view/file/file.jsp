<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="page" tagdir="/WEB-INF/tags" %>

<page:template>
    <jsp:body>
        <!-- Page Content -->
        <section class="about" id="about">
            <div class="container text-xs-center">

                <!-- Page Heading/Breadcrumbs -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Пример загрузки файла
                            <small>pdf или excel</small>
                        </h2>
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a>
                            </li>
                            <li class="active">Пример загрузки файла</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <c:url value="/uploadFile" var="fileUploadControllerURL"/>
                <!-- Content Row -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-lg-6">
                                <h3>Пример загрузки файла с помощью Spring MVC </h3>
                                <form action="${fileUploadControllerURL}" method="post"
                                      enctype="multipart/form-data">
                                    <table class="table">
                                        <tr>
                                            <td><b>File:</b></td>
                                            <td><input type="file" name="file"></td>
                                            <td><input type="submit" value="загрузить файл"></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>

                            <div class="col-lg-6">
                                <h3>Download Java Generated fail.</h3>

                                <c:url value="/excel" var="excelController"/>
                                <c:url value="/pdf" var="pdfController"/>
                                <a href="${excelController}">Excel</a>
                                <br/>
                                <a href="${pdfController}">PDF</a>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.row -->

                <hr>

            </div>
        </section>
        <!-- /.container -->
    </jsp:body>
</page:template>